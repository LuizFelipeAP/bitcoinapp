//
//  BitcoinTest.swift
//  BitcoinAppTests
//
//  Created by Luiz Felipe Albernaz Pio on 28/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import XCTest
@testable import BitcoinApp

class BitcoinTest: XCTestCase {

    let viewModel = BitcoinViewModel()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
 
    func test_Parseing() {
        //Happy Ending
        let q1 = Quotation(price: 14119.028333333334, dateUnix: 1514160000)
        let expected = Market.init(values: [q1])
        
        // Input
        let jsonString = "{\"values\": [{\"x\": 1514160000,\"y\": 14119.028333333334}]}"
        
        let jsonData = jsonString.data(using: .utf8)
        
        let result = self.viewModel.parseMarketPrices(from: jsonData!)
        
        //Test
        XCTAssertEqual(1, result.count)
        XCTAssertEqual(expected.values.first?.price, result.first?.price)
        XCTAssertEqual(expected.values.first?.dateUnix, result.first?.dateUnix)
        
        
    }
    
}
