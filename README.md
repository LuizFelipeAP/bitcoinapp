# O que é:

> Um aplicativo que mostra a evolução da moeada mais popular dos últimos tempos.
	
# O que usa: 
- Cocoapods: Para gerenciar dependências.
- RxSwift/RxCocoa: Para operações asincronas e bind dos dados com os compnenetes de interface.
- Alamofire: Para requisições com APIs.
- RealmSwift: Para persistencia local dos dados.
- Charts: Para Geração de gráficos.