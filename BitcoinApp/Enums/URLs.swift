//
//  URLs.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation

enum URLs: String {
    //Quotation URLs
    case QUOTATION_URL = "https://api.blockchain.info/stats"
    case MARKET_PRICES_URL = "https://api.blockchain.info/charts/market-price?timespan=8weeks"

    //Currency URLs
    case CURRENCY_URL = "https://api.fixer.io/latest?base=USD"

}
