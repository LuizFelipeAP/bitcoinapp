//
//  ResponseStatus.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation

///The common errors triggered by a request
enum RequestError: Error {
    case requestFailure
    case noResponse
    case noData
    case invalidURL
}

///The possibles outcomes of the request
enum ResponseStatus {
    case failure(RequestError)
    case success(Data)
}
