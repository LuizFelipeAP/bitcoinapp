//
//  BitcoinViewModel.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation
import RxSwift

class BitcoinViewModel {
    
    //MARK:- Properties
    private var currentQuotation = Variable<Double>(0)
    lazy var currentQuotationObservable = self.currentQuotation.asObservable()
    
    private var marketPrices = Variable<[Quotation]>([])
    lazy var marketPricesObservable = self.marketPrices.asObservable()
    
    private var brlValue = PublishSubject<Double>()
    
    private let networkManager = NetworkManager()
    private let daoManager = DAOManager.shared
    
    private let disposeBag = DisposeBag()
    
    init() {

    }
    
}

//**************************************************************************************
//
// MARK:- Methods Extension
//
//**************************************************************************************
extension BitcoinViewModel {
    
    public func getQuotations() {
        
        CurrencyManager.shared.fetchBRLValue { brlValue in
            self.brlValue.onNext(brlValue)
        }
        
        brlValue.subscribe(onNext: { _ in
            self.getBitcoinQuotation()
            self.getPastMonthMarketPrices()
        })
        .disposed(by: self.disposeBag)
    }
    
    private func getBitcoinQuotation() {
        
        self.networkManager.request(atEndPoint: URLs.QUOTATION_URL) { (response) -> (Void) in
            
            switch response {
            case .success(let data):
                let quotationUSD = self.parseQuotation(from: data)
                
                self.currentQuotation.value = quotationUSD

            case .failure(let error):
                print(error)
                //Display some thing
            }
        }
    }
    
    private func getPastMonthMarketPrices() {
        
        self.networkManager.request(atEndPoint: URLs.MARKET_PRICES_URL) { (response) -> (Void) in
            switch response {
            case .success(let data):
                let marketPrices = self.parseMarketPrices(from: data)
                self.marketPrices.value = marketPrices
                
                self.daoManager.clearTable()
                self.daoManager.saveList(objects: marketPrices)
                
            case .failure(let error):
                print(error)
                //Display some thing
            }
        }
    }
    
    private func parseQuotation(from data: Data) -> Double {
        let decoder = JSONDecoder()
        
        do {
            return try decoder.decode(CurrentQuotation.self, from: data).price
        } catch {
            return 0
        }
    }
    
    func parseMarketPrices(from data: Data) -> [Quotation] {
        let decoder = JSONDecoder()
        
        do {
            let marketPrices = try decoder.decode(Market.self, from: data).values.reversed()
           
            return Array(marketPrices)
            
        } catch {
            return []
        }
    }
    
    private func convertUSDToBRL(_ usd: Double) -> Double {
        let currencyValue = CurrencyManager.shared.getValue()
        return usd * currencyValue
    }
    
    func statusForQuotationAt(_ index: Int) -> QuotationStatus {
        
        let currentQuotation = self.marketPrices.value[index]
        guard let previousQuotation = self.marketPrices.value.get(at: index + 1) else {
            return .blue
        }
        
        if currentQuotation.price > previousQuotation.price {
            return .green
        } else if currentQuotation.price < previousQuotation.price {
            return .red
        } else {
            return .blue
        }
    }
    
    func displayablePrice(_ price: Double) -> String {
        let currencyValue = CurrencyManager.shared.getValue()
        let currencySymbol = currencyValue == 1 ? "$: " : "R$: "

        let finalPrice = convertUSDToBRL(price)
        
        return "\(currencySymbol)\(finalPrice.formattedWithSeparator)"
    }
    
    func quotationDisplayable(at index: Int) -> (String, String, QuotationStatus) {
        
        let quotation = self.marketPrices.value[index]
        
        let priceStr = self.displayablePrice(quotation.price)
        
        let dateStr = quotation.date()?.asString() ?? "Sem data"
        
        let status = self.statusForQuotationAt(index)
        
        return (priceStr, dateStr, status)
    }
}
