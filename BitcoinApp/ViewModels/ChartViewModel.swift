//
//  ChartViewModel.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 27/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation
import Charts

class CharViewModel {

    private var daoManager = DAOManager.shared
    
    var displayableData: [ChartDataEntry] = []
    
    init() {
        
    }
    
    func getChartData() -> LineChartData {
        let quotations = self.getDataFromDB()
                
        let qty = quotations.count >= 7 ? 7 : quotations.count
        
        print("Count: ", quotations.count)
        
        for i in 1...qty {
            
            let quotation = quotations[qty-i]
            
            let price = CurrencyManager.shared.getValue() * quotation.price
            let dateDay = quotation.date()?.asString(format: "dd")
            let day = dateDay ?? "0"
            let entry = ChartDataEntry(x: Double(day)!, y: price)
            displayableData.append(entry)
        }
        
        let line = LineChartDataSet(values: displayableData, label: "Cotação R$")
        line.colors = [.blue]
        
        let data = LineChartData()
        data.addDataSet(line)
        
        return data
    }
    
    private func getDataFromDB() -> [Quotation] {
        return self.daoManager.getElements()
    }
    
}
