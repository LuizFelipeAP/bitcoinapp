//
//  Quotation.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation
import RealmSwift

//Model for Bitcoin Quotation.
class Quotation: Object, Codable {
    //Price is gettered in USD
    @objc dynamic var price: Double = 0
    @objc dynamic var dateUnix: Double = 0
    
    enum CodingKeys: String, CodingKey {
        case price = "y"
        case dateUnix = "x"
    }
    
    convenience init(price: Double, dateUnix: Double) {
        self.init()
        self.price = price
        self.dateUnix = dateUnix
    }
    
    func date() -> Date? {
        return Date(timeIntervalSince1970: self.dateUnix)
    }
}

struct CurrentQuotation: Codable {
    
    var price: Double
    
    enum CodingKeys: String, CodingKey {
        case price = "market_price_usd"
    }
}
