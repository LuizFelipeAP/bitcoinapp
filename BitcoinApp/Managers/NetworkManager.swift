//
//  NetworkManager.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation
import Alamofire

///Responsable for make requests
class NetworkManager {
    
    //MARK:- Methods
    
    /**
     Make a request for the `blockchain` API requesting the statistics about the BTN market
     
     - parameters:
     - completion: A completion handler called after the request completes.
     
     */
    func request(atEndPoint endPoint: URLs, _ completion: @escaping(ResponseStatus) -> (Void)) {
        
        guard let url = URL(string: endPoint.rawValue) else {
            completion(.failure(.invalidURL))
            return
        }
        
        Alamofire.request(url).responseData { responseData in
            
            guard let response = responseData.response else {
                completion(.failure(.noResponse))
                return
            }
            
            if 200..<300 ~= response.statusCode {
             
                guard let data = responseData.data else {
                    completion(.failure(.noData))
                    return
                }
                
                completion(.success(data))
                
            } else {
                completion(.failure(.requestFailure))
            }
        }
    }
}
