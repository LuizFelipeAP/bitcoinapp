//
//  DAOManager.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import RealmSwift

protocol DAOHandler {
    associatedtype ObjectPersistent
    
    func saveList(objects: [ObjectPersistent])
    func getElements() -> [ObjectPersistent]
    func clearTable()
}

class DAOManager: DAOHandler {
    typealias ObjectPersistent = Quotation
    
    static let shared = DAOManager()
    
    private var realm: Realm
    
    private init() {
        self.realm = try! Realm()
    }
    
    func saveList(objects: [Quotation]) {
        do {
            try self.realm.write {
                self.realm.add(objects)
            }
        } catch {
            print(error)
        }
    }
    
    func getElements() -> [Quotation] {
        return Array(self.realm.objects(Quotation.self))
    }
    
    func clearTable() {
        try? self.realm.write {
            self.realm.delete(self.getElements())
        }
    }
}
