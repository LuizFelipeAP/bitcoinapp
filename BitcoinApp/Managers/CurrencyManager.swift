//
//  CurrencyManager.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation
import RxSwift

struct Rates: Codable {
    var BRL: Double
}

struct Currencies: Codable {
    var rates: Rates
}

class CurrencyManager {
    
    /**
     1 - Verifica se é nullo,
     2 - se for nullo, fas o request,
     3 - se não for nullo retorna o preco.
 */

    static let shared = CurrencyManager()
    private let networkManager = NetworkManager()
    
    private var currencyValue = Variable<Double>(1)
    lazy var valueObservable = self.currencyValue.asObservable()
    
    private init() {
        
        convertUSDToBRL()
    }
    
    func getValue() -> Double {
        return self.currencyValue.value
    }
    
    func fetchBRLValue(_ completion: @escaping (Double) -> (Void)) {
        
        self.networkManager.request(atEndPoint: .CURRENCY_URL) { (response) -> (Void) in
            
            switch response {
            case .success(let data):
                let brl = self.parseCurrencies(data)
                completion(brl)
            default:
                completion(1)
                break;
            }
        }
    }
    
    func convertUSDToBRL() {
        self.networkManager.request(atEndPoint: .CURRENCY_URL) { (response) -> (Void) in
            
            switch response {
            case .success(let data):
                let brl = self.parseCurrencies(data)
                self.currencyValue.value = brl
            default:
                self.currencyValue.value = 1
                break;
            }
        }
    }
    
    private func parseCurrencies(_ data: Data) -> Double {
        
        let decoder = JSONDecoder()
        
        do {
            return try decoder.decode(Currencies.self, from: data).rates.BRL
        } catch {
            return 1
        }
        
    }
}
