//
//  BitcoinViewController.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BitcoinViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentCurrencyLabel: UILabel!
    @IBOutlet weak var currentQuotationView: UIView!
    
    @IBOutlet weak var reloadData: UIButton!
    @IBOutlet weak var showChartButton: UIButton!
    
    private var viewModel = BitcoinViewModel()
    private var disposeBag = DisposeBag()
    
    private let loadingView = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.customizeCurrentCotationView()
        
        self.showLoadingView()
        
        bindUI()
        self.viewModel.getQuotations()
    }
    
    private func bindUI() {
        
        //Bind Tableview to model
        self.viewModel.marketPricesObservable
            .observeOn(MainScheduler.instance)
            .filter({ !$0.isEmpty })
            .bind(to: self.tableView.rx.items) { tableView, index, element in
                guard let cell = tableView.dequeueReusableCell(withIdentifier: QuotationTableViewCell.reuseID) as? QuotationTableViewCell else { return UITableViewCell() }
                
                let quotation = self.viewModel.quotationDisplayable(at: index)
                cell.setupCellWith(quotation: quotation)
                
                return cell
            }
            .disposed(by: self.disposeBag)
        
        //Hide loading view when data is fetched
        self.viewModel.marketPricesObservable
            .observeOn(MainScheduler.instance)
            .filter({ !$0.isEmpty })
            .subscribe(onNext: { _ in
                self.hideLoadingView()
            })
            .disposed(by: self.disposeBag)
        
        //Bind current bitcoint value
        self.viewModel.currentQuotationObservable.asObservable()
            .observeOn(MainScheduler.instance)
            .map { return "\(self.viewModel.displayablePrice($0))" }
            .bind(to: self.currentCurrencyLabel.rx.text)
            .disposed(by: self.disposeBag)
        
        self.showChartButton.rx
            .tap
            .throttle(0.5, scheduler: MainScheduler.instance)
            .subscribe(onNext: {
                self.presentChart()
            })
            .disposed(by: self.disposeBag)
        
        self.reloadData.rx
            .tap
            .throttle(1, scheduler: MainScheduler.instance)
            .subscribe(onNext: {
                self.viewModel.getQuotations()
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.reloadData.imageView?.transform = CGAffineTransform(rotationAngle: .pi)
                })
                
                UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                    self.reloadData.imageView?.transform = CGAffineTransform(rotationAngle: .pi * 2)
                }, completion: nil)
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupTableView() {
        
        self.tableView.delegate = self
        
        let nib = UINib(nibName: QuotationTableViewCell.reuseID, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: QuotationTableViewCell.reuseID)
    }
    
    private func presentChart() {
        let storyboard = UIStoryboard(name: "ChartViewController", bundle: nil)
        guard let chartVC = storyboard.instantiateInitialViewController() as? ChartViewController else { return }
        
        self.present(chartVC, animated: true, completion: nil)
    }
    
    private func customizeCurrentCotationView() {
        
        self.currentQuotationView.backgroundColor = .yellow
    }
    
    func showLoadingView() {
        self.tableView.setBackgroundWith(view: self.loadingView)
        self.loadingView.start()
    }
    
    func hideLoadingView() {
        self.tableView.removeBackgroundView()
        self.loadingView.stop()
    }
}

//**************************************************************************************
//
// MARK:- UITableViewDelegate Extension
//
//**************************************************************************************
extension BitcoinViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
