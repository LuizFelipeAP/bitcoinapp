//
//  QuotationTableViewCell.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 27/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import UIKit

enum QuotationStatus {
    case blue
    case green
    case red
    
    func image() -> UIImage {
        switch self {
        case .blue: return UIImage(named: "blue")!
        case .green: return UIImage(named: "green")!
        case .red: return UIImage(named: "red")!
        }
    }
}

class QuotationTableViewCell: UITableViewCell {

    class var reuseID: String {
        return "QuotationTableViewCell"
    }
    
    //MARK:- OUTLETS
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellWith(quotation: (String, String, QuotationStatus)) {
        
        self.priceLabel.text = "\(quotation.0)"
        self.dateLabel.text = quotation.1
        self.statusImage.image = quotation.2.image()
    }
    
}
