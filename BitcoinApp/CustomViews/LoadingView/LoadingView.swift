//
//  LoadingView.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 28/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)
        self.addSubview(containerView)
        self.containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func start() {
        self.activityIndicator.startAnimating()
        self.isHidden = false
    }
    
    func stop() {
        self.activityIndicator.stopAnimating()
        self.isHidden = true
    }
}
