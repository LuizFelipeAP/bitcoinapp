//
//  Double+Util.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 26/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation

extension Double {
    var formattedWithSeparator: String {
        let str = Formatter.withSeparator.string(for: self) ?? ""
        return str.count > 0 ? str.suffix(str.count - 1).description : ""
    }
}
