//
//  Array+Util.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 27/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import Foundation

extension Array {
    
    func get(at index: Int) -> Element? {
        return 0..<self.count ~= index ? self[index] : nil
    }
}
