//
//  UITableView+Extension.swift
//  BitcoinApp
//
//  Created by Luiz Felipe Albernaz Pio on 28/01/18.
//  Copyright © 2018 Luiz Felipe Albernaz Pio. All rights reserved.
//

import UIKit

extension UITableView {
    
    func setBackgroundWith(view: UIView) {
        self.backgroundView = view
        self.separatorStyle = .none
    }
    
    func removeBackgroundView(separator: UITableViewCellSeparatorStyle = .singleLine) {
        self.backgroundView = nil
        self.separatorStyle = separator
    }
}
